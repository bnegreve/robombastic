package fr.dauphine.robombastic.example;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import fr.umlv.lawrence.Application;
import fr.umlv.lawrence.DefaultGridModel;
import fr.umlv.lawrence.GridPane;
import fr.umlv.lawrence.InputListener;
import fr.umlv.lawrence.Key;
import fr.umlv.lawrence.svg.SVGImageProvider;

public class Arena {

	private final DefaultGridModel<ArenaImage> model; 
	private static final int NB_TILE_X = 20;
	private static final int NB_TILE_Y = 20;
	private static final int TILE_WIDTH = 30;
	private static final int TILE_HEIGHT = 30;
	private final GridPane<ArenaImage> pane; 
	private final Map<String, ArenaImage> allImages = new HashMap<>();	

	public Arena(){
		
		/* Set up the arena and the grid based display */ 
		 model=new DefaultGridModel<>(NB_TILE_X, NB_TILE_Y); 
		 SVGImageProvider<ArenaImage> images = new SVGImageProvider<>();		 
		 images.setDPI(93);
		 
		 /* Load all images */ 
		 allImages.put("frog", new ArenaImage(images, "grenouille"));
		 allImages.put("bg", new ArenaImage(images, "fond"));
		 
		 pane = new GridPane<>(model,images,TILE_WIDTH, TILE_HEIGHT);

		 drawGrid();
	}

	
	private void drawGrid() {
		    for(int x=0;x<model.getWidth();x++) {
		      for(int y=0;y<model.getHeight();y++) {
		    	  model.addDeffered(x, y, allImages.get("bg"));
		      }
		    }
		    model.swap();
	}	
	
	public Runnable getRunnable(){
		return new Runnable(){
			public void run(){
				drawGrid();								
				model.addDeffered(new Random().nextInt(NB_TILE_X), new Random().nextInt(NB_TILE_Y), allImages.get("frog"));					
				
				model.swap();
			}
		};
	}
	
	  void display() {		 
		  Application.display(pane, "Projet java", false, true);
		  
		  final ScheduledFuture<?> periodicUpdate = Application.scheduleWithFixedDelay(getRunnable(),0,500,TimeUnit.MILLISECONDS);
		  
		  pane.addInputListener(new InputListener(){

				@Override
				public void keyTyped(int arg0, int arg1, Key arg2) {
					if(arg2.equals(Key.ESCAPE)){
						System.out.println("escape key pressed!");
						periodicUpdate.cancel(true);
						Application.close(pane);
						System.exit(0); 
					}					
				}

				@Override
				public void mouseClicked(int arg0, int arg1, int arg2) {							
				}				 
			 });	
		  }
	

	public static void main(String[] args) {

			Arena arena = new Arena();
			arena.display(); 

	}

}
